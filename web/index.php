

<?php

use Slim\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use FoodTracker\Routing\RoutingConstants;
use Slim\Routing\RouteContext;
use FoodTracker\Session\SessionHelpers;
use FoodTracker\Model\Item;
use FoodTracker\Model\ItemBuilder;

$authMiddleware = function (Request $request, RequestHandler $handler){
    $routeContext = RouteContext::fromRequest($request);
    $route = $routeContext->getRoute();

    // return NotFound for non existent route
    if (empty($route)) {
        throw new NotFoundException($request, $response);
    }

    $routeName = $route->getName();
    $response = new Response();
    $allowedRoutes = RoutingConstants::getAllowedRoutes();

    if((!isset($_SESSION['loggedin']) or !$_SESSION['loggedin']) and (!isset($allowedRoutes[$routeName]))){
        $_SESSION['accessDenied'] = true;
        SessionHelpers::clear_duplicate_cookies();
        $response = $response->withStatus(302);
        return $response->withHeader('Location', RoutingConstants::ALL_ROUTES[RoutingConstants::LOGIN_GET_NAME]);
    }
    else{
        if(isset($_SESSION['accessDenied']) and $_SESSION['accessDenied']){
            $request = $request->withAttribute('accessDenied', true);
            $_SESSION['accessDenied'] = false;
            SessionHelpers::clear_duplicate_cookies();
        }
        
        return $handler->handle($request);
    }
};

$container = require __DIR__ . '/../app/bootstrap.php';

session_start();
SessionHelpers::clear_duplicate_cookies();

$app = \DI\Bridge\Slim\Bridge::create($container);

//$app->setBasePath('/food-tracker-php/web');

$app->get('/hello/{name}', [FoodTracker\Controller\ItemsController::class,'get']);
$app->post(RoutingConstants::ALL_ROUTES[RoutingConstants::LOGIN_POST_NAME],[FoodTracker\Controller\LoginController::class,'post'])->setName(RoutingConstants::LOGIN_POST_NAME);
$app->get(RoutingConstants::ALL_ROUTES[RoutingConstants::LOGIN_GET_NAME],[FoodTracker\Controller\LoginController::class,'get'])->setName(RoutingConstants::LOGIN_GET_NAME);
$app->get(RoutingConstants::ALL_ROUTES[RoutingConstants::REGISTER_GET_NAME],[FoodTracker\Controller\RegisterController::class,'get'])->setName(RoutingConstants::REGISTER_GET_NAME);
$app->post(RoutingConstants::ALL_ROUTES[RoutingConstants::REGISTER_POST_NAME],[FoodTracker\Controller\RegisterController::class,'post'])->setName(RoutingConstants::REGISTER_POST_NAME);
$app->get(RoutingConstants::ALL_ROUTES[RoutingConstants::HOME_GET_NAME],[FoodTracker\Controller\HomeController::class,'get'])->setName(RoutingConstants::HOME_GET_NAME);
$app->get(RoutingConstants::ALL_ROUTES[RoutingConstants::ROOT_GET_NAME],[FoodTracker\Controller\HomeController::class,'get'])->setName(RoutingConstants::ROOT_GET_NAME);
$app->get(RoutingConstants::ALL_ROUTES[RoutingConstants::FOODJOURNAL_GET_NAME],[FoodTracker\Controller\FoodJournalController::class,'get'])->setName(RoutingConstants::FOODJOURNAL_GET_NAME);
$app->get(RoutingConstants::ALL_ROUTES[RoutingConstants::NEW_ITEM_GET_NAME],[FoodTracker\Controller\NewItemController::class,'get'])->setName(RoutingConstants::NEW_ITEM_GET_NAME);
$app->get(RoutingConstants::ALL_ROUTES[RoutingConstants::DISPLAY_ITEM_GET_NAME],[FoodTracker\Controller\DisplayItemController::class,'get'])->setName(RoutingConstants::DISPLAY_ITEM_GET_NAME);
$app->post(RoutingConstants::ALL_ROUTES[RoutingConstants::NEW_ITEM_POST_NAME],[FoodTracker\Controller\NewItemController::class,'post'])->setName(RoutingConstants::NEW_ITEM_POST_NAME);

$app->add($authMiddleware);

$app->addRoutingMiddleware();

$app->run();

