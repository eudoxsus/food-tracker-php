﻿## php-food-tracker

A Food Tracker project to learn how to implement a rapid-development website on a WAMP stack.

Even though development is fast on this stack, dev principles are still scaleable with MVC and dependency-injection principles used extensively.
