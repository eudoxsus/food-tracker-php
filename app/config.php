<?php

use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use FoodTracker\Persistence\idbConnection;
use FoodTracker\Persistence\dbConnection;
use FoodTracker\Persistence\Repo\iItemRepository;
use FoodTracker\Persistence\Repo\ItemRepository;
use FoodTracker\Persistence\Repo\iUserRepository;
use FoodTracker\Persistence\Repo\UserRepository;
use FoodTracker\Persistence\Repo\iFatRepository;
use FoodTracker\Persistence\Repo\FatRepository;
use FoodTracker\Persistence\Repo\iCarbRepository;
use FoodTracker\Persistence\Repo\CarbRepository;

return [
    // Bind an interface to an implementation
    idbConnection::class => DI\autowire(dbConnection::class),
    iItemRepository::class => DI\autowire(ItemRepository::class),
    iUserRepository::class => DI\autowire(UserRepository::class),
    iCarbRepository::class => DI\autowire(CarbRepository::class),
    iFatRepository::class => DI\autowire(FatRepository::class),

    // Configure Twig
    Environment::class => function () {
        $loader = new FilesystemLoader(__DIR__ . '/../src/FoodTracker/Views');
        return new Environment($loader,array('auto_reload' => true));
    },
];
