<?php

namespace FoodTracker\Routing;

class RoutingConstants
{

    private static array $ALLOWED_ROUTES;

    const BASEPATH = '/food-tracker-php/web';

    const LOGIN_GET_NAME = 'loginGet';
    const LOGIN_POST_NAME = 'loginPost';
    const REGISTER_GET_NAME = 'registerGet';
    const REGISTER_POST_NAME = 'registerPost';
    const HOME_GET_NAME = 'homeGet';
    const ROOT_GET_NAME = 'rootGet';
    const FOODJOURNAL_GET_NAME = 'foodJournalsGet';
    const DISPLAY_ITEM_GET_NAME = 'displayItemsGet';
    const DISPLAY_ITEM_POST_NAME = 'displayItemsPost';
    const NEW_ITEM_GET_NAME = 'newItemGet';
    const NEW_ITEM_POST_NAME = 'newItemPost';

    const ALL_ROUTES = [
        RoutingConstants::LOGIN_GET_NAME => (RoutingConstants::BASEPATH . '/login'),
        RoutingConstants::LOGIN_POST_NAME => (RoutingConstants::BASEPATH . '/login'),
        RoutingConstants::REGISTER_GET_NAME => (RoutingConstants::BASEPATH . '/register'),
        RoutingConstants::REGISTER_POST_NAME => (RoutingConstants::BASEPATH . '/register'),
        RoutingConstants::HOME_GET_NAME => (RoutingConstants::BASEPATH . '/home'),
        RoutingConstants::ROOT_GET_NAME => (RoutingConstants::BASEPATH . '/'),
        RoutingConstants::FOODJOURNAL_GET_NAME => (RoutingConstants::BASEPATH . '/foodjournals'),
        RoutingConstants::DISPLAY_ITEM_GET_NAME => (RoutingConstants::BASEPATH . '/items'),
        RoutingConstants::DISPLAY_ITEM_POST_NAME => (RoutingConstants::BASEPATH . '/items'),
        RoutingConstants::NEW_ITEM_GET_NAME => (RoutingConstants::BASEPATH . '/newitem'),
        RoutingConstants::NEW_ITEM_POST_NAME => (RoutingConstants::BASEPATH . '/newitem'),
    ];

    private static $ALLOWED_ROUTE_NAMES = 
    [RoutingConstants::LOGIN_GET_NAME,
    RoutingConstants::LOGIN_POST_NAME,
    RoutingConstants::REGISTER_GET_NAME,
    RoutingConstants::REGISTER_POST_NAME];

    public static function getAllowedRoutes() : array{
        if(!isset(self::$ALLOWED_ROUTES)){
            self::$ALLOWED_ROUTES = [];
            foreach(self::$ALLOWED_ROUTE_NAMES as $name){
                self::$ALLOWED_ROUTES[$name] = self::ALL_ROUTES[$name];
            }
        }

        return self::$ALLOWED_ROUTES;
    }



}