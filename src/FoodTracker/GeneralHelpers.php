<?php

namespace FoodTracker;

/**
 * Represents a User
 */
class GeneralHelpers{

    // Function to check string starting 
    // with given substring 
    static function startsWith ($string, $startString) 
    { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    } 
    
}