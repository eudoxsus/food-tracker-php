<?php

namespace FoodTracker\Session;

class SessionHelpers{

    /**
    * Every time you call session_start(), PHP adds another
    * identical session cookie to the response header. Do this
    * enough times, and your response header becomes big enough
    * to choke the web server.
    *
    * This method clears out the duplicate session cookies. You can
    * call it after each time you've called session_start(), or call it
    * just before you send your headers.
    */
    static function clear_duplicate_cookies() {
        // If headers have already been sent, there's nothing we can do
        if (headers_sent()) {
            return;
        }
    
        $cookies = array();
        foreach (headers_list() as $header) {
            // Identify cookie headers
            if (strpos($header, 'Set-Cookie:') === 0) {
                $cookies[] = $header;
            }
        }
        // Removes all cookie headers, including duplicates
        header_remove('Set-Cookie');
    
        // Restore one copy of each cookie
        foreach(array_unique($cookies) as $cookie) {
            header($cookie, false);
        }
    }

}