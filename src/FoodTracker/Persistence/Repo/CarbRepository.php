<?php

namespace FoodTracker\Persistence\Repo;

use FoodTracker\Persistence\Repo\iCarbRepository;
use FoodTracker\Persistence\idbConnection;
use FoodTracker\Model\Carb;
use FoodTracker\Model\CarbBuilder;

class CarbRepository implements iCarbRepository{

    private idbConnection $dbConnection;

    public function __construct(idbConnection $dbConnection){
        $this->dbConnection = $dbConnection;
    }

    public function getCarb(int $id){
        $stmt = $this->dbConnection->prepare('SELECT id,foodId,fiber,sugar,other FROM carbohydrates WHERE id = ?');
        $stmt->bindParam(1,$id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        if($res){
            $carbBuilder = new CarbBuilder();
            $carbBuilder->setId($res['id'])->setFoodId($res['foodId'])->setFiber($res['fiber'])->setSugar($res['sugar'])->setOther($res['other']);
            return new Carb($carbBuilder);
        }
        else{

        }
    }

    public function updateCarb(Carb $carb){
        $stmt = $this->dbConnection->prepare("update carbohydrates set foodId = ?,fiber = ?, sugar = ?,other = ? where id = ?");
        $stmt->bindValue(1,$carb->getFoodId(),\PDO::PARAM_INT);
        $stmt->bindValue(2,strval($carb->getFiber()));
        $stmt->bindValue(3,strval($carb->getSugar()));
        $stmt->bindValue(4,strval($carb->getOther()));
        $stmt->bindValue(5,strval($carb->getId()),\PDO::PARAM_INT);
        $stmt->execute();
        
    }

    public function insertCarb(Carb $carb,bool $returnObj = false):array{
        if($carb->getFoodId() !== -1){
            $stmt = $this->dbConnection->prepare("INSERT INTO carbohydrates (foodId, fiber, sugar, other) VALUES(?,?,?,?)");
            //$stmt->bind_param('iddd',$carb->getFoodId(),$carb->getFiber(),$carb->getSugar(),$carb->getOther());
            $stmt->bindValue(1,$carb->getFoodId(),\PDO::PARAM_INT);
            $stmt->bindValue(2,strval($carb->getFiber()));
            $stmt->bindValue(3,strval($carb->getSugar()));
            $stmt->bindValue(4,strval($carb->getOther()));
        }
        else{
            $stmt = $this->dbConnection->prepare("INSERT INTO carbohydrates (fiber, sugar, other) VALUES(?,?,?)");
            //$stmt->bind_param('ddd',$carb->getFiber(),$carb->getSugar(),$carb->getOther());
            $stmt->bindValue(1,strval($carb->getFiber()));
            $stmt->bindValue(2,strval($carb->getSugar()));
            $stmt->bindValue(3,strval($carb->getOther()));
        }
        $stmt->execute();
        if($stmt->errorCode() === '00000'){
            $stmt->closeCursor();
            $insertedId = $this->dbConnection->getInsertId();
            if($returnObj){
                $carbBuilder = new CarbBuilder();
                $carbBuilder->copyCarb($carb)->setId($insertedId);
                $insertedCarb = new Carb($carbBuilder);
                return ['isSuccessful' => true,'resultObj' => $insertedCarb];
            }
            else{
                return ['isSuccessful' => true,'resultId' => $insertedId];
            }
        }
        else{
            $stmt->closeCursor();
            return ['isSuccessful' => false,'errorCode' => $stmt->errorCode(),'errorMsg' => $stmt->errorInfo()];
        }
            

    }
}