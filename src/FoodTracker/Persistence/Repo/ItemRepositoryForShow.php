<?php

namespace FoodTracker\Persistence\Repo;

use FoodTracker\Persistence\Repo\iItemRepository;
use FoodTracker\Persistence\Repo\iFatRepository;
use FoodTracker\Persistence\Repo\iCarbRepository;
use FoodTracker\Persistence\idbConnection;
use FoodTracker\Model\Item;
use FoodTracker\Model\ItemBuilder;
use FoodTracker\Model\Fat;
use FoodTracker\Model\FatBuilder;
use FoodTracker\Model\Carb;
use FoodTracker\Model\CarbBuilder;

class ItemRepository implements iItemRepository{

    public function getItemsQuery(string $query):array{
        $items = [];
        $stmt = $this->dbCon->prepare("select * from items $query");
        $stmt->execute();
        
        while($res = $stmt->fetch(\PDO::FETCH_ASSOC)){            
            foreach (ItemBuilder::SET_METHOD_REFS 
            as $prop => $method){
                $itemBuilder[$prop] = $res[$prop];
            }
            $items[] = new Item($itemBuilder);
        }
        return $items;
    }





    public function __construct(idbConnection $dbCon,iCarbRepository $carbRepo,iFatRepository $fatRepo){
        $this->dbCon = $dbCon;
        $this->carbRepo = $carbRepo;
        $this->fatRepo = $fatRepo;
        $this->dbFieldCount = \count(Item::GET_METHOD_REFS);
    }

    public function updateItem(Item $item){
        $setQuery = 'update items set ';
        $first = true;
        foreach (Item::GET_METHOD_REFS as $field => $val){
            if($first){
                $first = false;
                $setQuery .= $field . ' = ? ';
                continue;
            }
            $setQuery .= ', ' . $field . ' = ?';
        }

        $setQuery .= 'where id = ?';
        //("update items set carbId = ?, fatId = ?, name = ?, calories = ?, protein = ?, foodUnit = ?, servingQty = ? where id = ?"
        $stmt = $this->dbCon->prepare($setQuery);
        $stmt->bindValue(1,$item->getCarb()->getId(),\PDO::PARAM_INT);
        $stmt->bindValue(2,$item->getFat()->getId(),\PDO::PARAM_INT);
        $stmt->bindValue(3,$item->getName());
        $stmt->bindValue(4,strval($item->getCalories()));
        $stmt->bindValue(5,strval($item->getProtein()));
        $stmt->bindValue(6,$item->getFoodUnit());
        $stmt->bindValue(7,strval($item->getServingQty()));
        $stmt->bindValue(8,$item->getId(),\PDO::PARAM_INT);
        $stmt->execute();
    }

    public function buildBindVal($stmt,$val,int $PDOParam = \PDO::PARAM_STR){
        if(isset($lastStmt) and $stmt === $lastStmt){
            $valCounter += 1;
        }else{
            $valCounter = 1;
        }
        $stmt->bindValue($valCounter,$val,$PDOParam);
    }

    public function insertItem(Item $item):array{
        $carbResult = $this->carbRepo->insertCarb($item->getCarb(),true);
        $fatResult = $this->fatRepo->insertFat($item->getFat(),true);
        if(!$carbResult['isSuccessful']){
            return $carbResult;
        }
        if(!$fatResult['isSuccessful']){
            return $fatResult;
        }
        $insertedFat = $fatResult['resultObj'];
        $insertedCarb = $carbResult['resultObj'];

        $first = true;
        $setQuery = 'INSERT INTO items (';
        foreach (Item::GET_METHOD_REFS as $field => $val){
            if($first){
                $first = false;
                $setQuery .= $field;
                continue;
            }
            $setQuery .= ',' . $field;
        }
        $first = true;
        $setQuery .= ') VALUES(';
        for ($x = 0;$x < $this->dbFieldCount;$x++){
            if($first){
                $first = false;
                $setQuery .= '?';
                continue;
            }

            $setQuery .= ',?';
        }
        $setQuery .= ')';

        $stmt = $this->dbCon->prepare($setQuery);
        $stmt->bindValue(1,$insertedCarb->getId(),\PDO::PARAM_INT);
        $stmt->bindValue(2,$insertedFat->getId(),\PDO::PARAM_INT);
        $stmt->bindValue(3,$item->getName());
        $stmt->bindValue(4,strval($item->getCalories()));
        $stmt->bindValue(5,strval($item->getProtein()));
        $stmt->bindValue(6,$item->getFoodUnit());
        $stmt->bindValue(7,strval($item->getServingQty()));
        $stmt->execute();
        if($stmt->errorCode() === '00000'){
            $stmt->closeCursor();
            $insertedId = $this->dbCon->getInsertId();

            $fatBuilder = new FatBuilder();
            $carbBuilder = new CarbBuilder();
            $fatBuilder->copyFat($insertedFat)->setFoodId($insertedId);
            $carbBuilder->copyCarb($insertedCarb)->setFoodId($insertedId);

            $this->carbRepo->updateCarb(new Carb($carbBuilder));
            $this->fatRepo->updateFat(new Fat($fatBuilder));

            $itemBuilder = new ItemBuilder($insertedCarb,$insertedFat);
            $itemBuilder->copyItem($item)->setId($insertedId);
            $insertedItem = new Item($itemBuilder);
            return ['isSuccessful' => true,'resultObj' => $insertedItem];
        }
        else{
            $stmt->closeCursor();
            return ['isSuccessful' => false,'errorCode' => $stmt->errorCode(),'errorMsg' => $stmt->errorInfo()];
        }

    }

    public function getItem(int $id):Item{
        
    }

    public function getItemsByName(string $name):array{
        
    }
}