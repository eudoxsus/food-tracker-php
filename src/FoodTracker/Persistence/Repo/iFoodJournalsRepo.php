<?php

namespace FoodTracker\Persistence\Repo;

use FoodTracker\Model\FoodJournal;
use FoodTracker\Model\FoodJournalEntry;
use FoodTracker\Model\FoodJournalEntryBuilder;

/**
 * Handles FoodJournals Database actions
 *
 */
interface iFoodJournalsRepository{

        
    /**
     * getFoodJournalEntry
     *
     * @param int $id
     * @return FoodTracker\Model\FoodJournalEntry
     */
    public function getFoodJournalEntryById(int $id);

    /**
     * getFoodJournalEntriesForDate
     *
     * @param int $userId
     * @param int $date
     * @return array [FoodTracker\Model\FoodJournalEntry]
     */
    public function getFoodJournalEntriesForDate(int $userId,int $date);
    
    /**
     * insertFoodJournal
     *
     * @param int $userId
     * @return array Array of boolean isSuccessful and string errorCode
     */
    public function insertFoodJournal(int $userId);

    /**
     * insertFoodJournal
     *
     * @param FoodTracker\Model\User $user
     * @return array Array of boolean isSuccessful and string errorCode
     */
    public function insertFoodJournalEntry(FoodJournalEntry $foodJournalEntry);

    /**
     * updateFoodJournal
     *
     * @param FoodTracker\Model\FoodJournal $foodJournal
     * @return array Array of boolean isSuccessful and string errorCode
     */
    public function updateFoodJournal(FoodJournal $foodJournal);

    /**
     * updateFoodJournalEntry
     *
     * @param FoodTracker\Model\FoodJournalEntry $foodJournalEntry
     * @return array Array of boolean isSuccessful and string errorCode
     */
    public function updateFoodJournalEntry(FoodJournalEntry $foodJournalEntry);

}