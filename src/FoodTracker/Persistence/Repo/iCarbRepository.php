<?php

namespace FoodTracker\Persistence\Repo;

use FoodTracker\Model\Carb;

/**
 * Handles Carb Database actions
 *
 */
interface iCarbRepository{

        
    /**
     * getCarbs
     *
     * @param int $id
     * @return FoodTracker\Model\Carb
     */
    public function getCarb(int $id);

    public function updateCarb(Carb $carb);
    
    /**
     * insertCarb
     *
     * @param FoodTracker\Model\Carb $carb
     * @return bool True if insert successful or false if failed
     */
    public function insertCarb(Carb $carb):array;

}