<?php

namespace FoodTracker\Persistence\Repo;

abstract class BaseRepository {

    /**
     * @var idbConnection
     */
    private idbConnection $dbCon;

    public function __construct(idbConnection $dbCon){
        $this->dbCon = $dbCon;
    }

    protected function checkStmtResult($stmt,callable $successFunc,callable $failureFunc):mixed{
        if($stmt->errorCode() === '00000'){
            $stmt->closeCursor();
            return \call_user_func($successFunc);
        }
        else{
            $stmt->closeCursor();
            return \call_user_func($failureFunc);
        }
    }

}