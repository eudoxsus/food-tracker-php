<?php

namespace FoodTracker\Persistence\Repo;

use FoodTracker\Persistence\Repo\iUserRepository;
use FoodTracker\Persistence\idbConnection;
use FoodTracker\Model\User;

class UserRepository implements iUserRepository{

    /**
     * @var idbConnection
     */
    private idbConnection $dbCon;

    public function __construct(idbConnection $dbCon){
        $this->dbCon = $dbCon;
    }

    public function getUserByName(string $username){
        $stmt = $this->dbCon->prepare('SELECT id,email,password FROM accounts WHERE username = ?');
        $stmt->bindParam(1,$username);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);

        if($res !== null){
            $stmt->closeCursor();
            return new User($res['id'],$username,$res['email'],$res['password']);
        }
        else{
            $stmt->closeCursor();
            return null;
        }
        
    }
    

    public function insertUser(string $username,string $password,string $email){
        try{
            $stmt = $this->dbCon->prepare("INSERT INTO accounts (username, password, email) VALUES(?, ?, ?)");
            $stmt->bindValue(1,$username);
            $stmt->bindValue(2,$password);
            $stmt->bindValue(3,$email);
            $stmt->execute();
            $stmt->closeCursor();
            return ['isSuccessful' => true];
        }catch(Exception $e){
            return ['isSuccessful' => false,'errorCode' => \PDO::errorCode()];
        }
        
    }
}