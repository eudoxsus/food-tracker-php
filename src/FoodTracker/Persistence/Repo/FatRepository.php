<?php

namespace FoodTracker\Persistence\Repo;

use FoodTracker\Persistence\Repo\iFatRepository;
use FoodTracker\Persistence\idbConnection;
use FoodTracker\Model\Fat;
use FoodTracker\Model\FatBuilder;

class FatRepository implements iFatRepository{

    private idbConnection $dbConnection;

    public function __construct(idbConnection $dbConnection){
        $this->dbConnection = $dbConnection;
    }

    public function getFat(int $id){
        $stmt = $this->dbConnection->prepare('SELECT id,foodId,saturated,unsaturated FROM fat WHERE id = ?');
        $stmt->bindParam(1,$id);
        $stmt->execute();
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);
        if($res){
            $fatBuilder = new FatBuilder();
            $fatBuilder->setId($res['id'])->setFoodId($res['foodId'])->setSaturated($res['saturated'])->setUnsaturated($res['unsaturated']);
            return new Fat($fatBuilder);
        }
        else{

        }
    }

    public function updateFat(Fat $fat){
        $stmt = $this->dbConnection->prepare("update fat set foodId = ?,saturated = ?, unsaturated = ? where id = ?");
        $stmt->bindValue(1,$fat->getFoodId(),\PDO::PARAM_INT);
        $stmt->bindValue(2,strval($fat->getSaturated()));
        $stmt->bindValue(3,strval($fat->getUnsaturated()));
        $stmt->bindValue(4,$fat->getId(),\PDO::PARAM_INT);
        $stmt->execute();
    }

    public function insertFat(Fat $fat,bool $returnObj = false):array{

        if($fat->getFoodId() !== -1){
            $stmt = $this->dbConnection->prepare("INSERT INTO fat (foodId,unsaturated,saturated) VALUES(?,?,?)");
            $stmt->bindValue(1,$fat->getFoodId(),\PDO::PARAM_INT);
            $stmt->bindValue(2,strval($fat->getUnsaturated()));
            $stmt->bindValue(3,strval($fat->getSaturated()));
        }
        else{
            $stmt = $this->dbConnection->prepare("INSERT INTO fat (unsaturated,saturated) VALUES(?,?)");
            $stmt->bindValue(1,strval($fat->getUnsaturated()));
            $stmt->bindValue(2,strval($fat->getSaturated()));
        }
        $stmt->execute();
        if($stmt->errorCode() === '00000'){
            $stmt->closeCursor();
            $insertedId = $this->dbConnection->getInsertId();
            if($returnObj){
                $fatBuilder = new FatBuilder();
                $fatBuilder->copyFat($fat)->setId($insertedId);
                $insertedFat = new Fat($fatBuilder);
                return ['isSuccessful' => true,'resultObj' => $insertedFat];
            }
            else{
                return ['isSuccessful' => true,'resultId' => $insertedId];
            }
        }
        else{
            $stmt->closeCursor();
            return ['isSuccessful' => false,'errorCode' => $stmt->errorCode(),'errorMsg' => $stmt->errorInfo()];
        }
    }
}