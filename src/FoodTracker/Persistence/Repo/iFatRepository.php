<?php

namespace FoodTracker\Persistence\Repo;

use FoodTracker\Model\Fat;

/**
 * Handles Fat Database actions
 *
 */
interface iFatRepository{

        
    /**
     * getFat
     *
     * @param int $id
     * @return FoodTracker\Model\Fat
     */
    public function getFat(int $id);

    public function updateFat(Fat $fat);
    
    /**
     * insertFat
     *
     * @param FoodTracker\Model\Fat $fat
     * @return Fat The inserted Fat object, now with an id
     */
    public function insertFat(Fat $fat):array;

}