<?php

namespace FoodTracker\Persistence\Repo;

/**
 * Handles User Database actions
 *
 */
interface iUserRepository{

        
    /**
     * getUser
     *
     * @param int $id
     * @return $stmt
     */
    public function getUserByName(string $username);
    
    /**
     * insertUser
     *
     * @param String $username
     * @param String $password
     * @param String $email
     * @return array Array of boolean isSuccesful and string errorCode
     */
    public function insertUser(string $username,string $password,string $email);

}