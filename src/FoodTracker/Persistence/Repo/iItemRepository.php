<?php

namespace FoodTracker\Persistence\Repo;
use FoodTracker\Model\Item;

/**
 * Handles Item Database actions
 *
 */
interface iItemRepository{

        
    /**
     * getItem
     *
     * @param int $id
     * @return FoodTracker\Model\Item
     */
    public function getItem(int $id):Item;

    /**
     * getItemsQuery
     *
     * @return array
     */
    public function getItemsQuery(string $query):array;

    public function updateItem(Item $item);

    /**
     * getItemsByName
     *
     * @param string $name
     * @return FoodTracker\Model\Item[]
     */
    public function getItemsByName(string $name):array;
    
    /**
     * insertItem
     *
     * @param FoodTracker\Model\Item $item
     * @return array Array of boolean isSuccesful and string errorCode
     */
    public function insertItem(Item $item):array;

}