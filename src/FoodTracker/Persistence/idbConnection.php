<?php

namespace FoodTracker\Persistence;

interface idbConnection{
    public function prepare(string $statement);
    public function getInsertId():int;
}