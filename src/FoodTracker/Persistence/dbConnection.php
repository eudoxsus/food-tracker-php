<?php

namespace FoodTracker\Persistence;

use FoodTracker\Persistence\idbConnection;

class dbConnection implements idbConnection {
    private $dbhost = 'localhost';
    private $dbuser = 'swizrc';
    private $dbpass = 'CelesSummo114';
    private $dbname = 'FoodTrackerPHP';
    private string $dsn; 
    private $conn;

    function __construct() {
        $this->dsn = "mysql:dbname=$this->dbname;host=$this->dbhost;port=3306;charset=utf8";
        $this->conn = new \PDO($this->dsn,$this->dbuser,$this->dbpass);
        $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        if(!$this->conn){
            die('Could not connect: ' . mysqli_error());
        }
    }

    function prepare(string $statement){
        return $this->conn->prepare($statement);
    }

    function getInsertId():int{
        return $this->conn->lastInsertId();
    }
}

?>