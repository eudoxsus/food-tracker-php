<?php

namespace FoodTracker\Controller;

use Twig_Environment;

abstract class BaseTwigController{

    /**
     * @var array $renderArr
     */
    private $renderArr;

    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var iSessionHelpers
     */
    private $sessHelpers;

    public function __construct(Twig_Environment $twig,iSessionHelpers $sessHelpers)
    {
        $this->twig = $twig;
        $this->sessHelpers = $sessHelpers;
    }

}