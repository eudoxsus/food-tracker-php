<?php

namespace FoodTracker\Controller;

use FoodTracker\Persistence\Repo\iItemRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Twig_Environment;
use FoodTracker\Model\Carb;
use FoodTracker\Model\Item;
use FoodTracker\Model\Fat;

class DisplayItemController{

    /**
     * @var iItemRepository Item Repository object
     */
    private iItemRepository $itemRepo;

    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var array $renderArr
     */
    private $renderArr = [];

    const VIEW_CONTENT_OBJS = [
        ['header' => 'Name','contentFunc' => 'getNameContent'],
        ['header' => 'Calories','contentFunc' => 'getCaloriesContent'],
        ['header' => 'Serving Quantity','contentFunc' => 'getServingQtyContent'],
        ['header' => 'Food Unit','contentFunc' => 'getFoodUnitContent'],
        ['header' => 'Protein','contentFunc' => 'getProteinContent'],
        ['header' => 'Carb','contentFunc' => 'getCarbContent'],
        ['header' => 'Fat','contentFunc' => 'getFatContent'],
    ];

    public function __construct(Twig_Environment $twig,iItemRepository $itemRepo)
    {
        $this->itemRepo = $itemRepo;
        $this->twig = $twig;
    }
    
    public function get(Request $request,Response $response){
        /*
        MODEL:
        {
            {
                tableObj{
                    headers : [(string)],
                    rows : [
                        {
                            (header) : (content)
                        }
                    ]
                }
            }
        }
        */
        $tableObj = ['headers' => array()];
        $tableObj['rows'] = [];
        $itemObjs = $this->itemRepo->getItemsQuery('');
        foreach(DisplayItemController::VIEW_CONTENT_OBJS as $viewObj){
            $tableObj['headers'][] = $viewObj['header'];
        }
        foreach($itemObjs as $item){
            $rowContent = [];
            foreach(DisplayItemController::VIEW_CONTENT_OBJS as $viewObj){
                $rowContent[$viewObj['header']] = ['content' => $this->{$viewObj['contentFunc']}($item)];
            }
            $tableObj['rows'][] = $rowContent;
        }
        $this->renderArr['tableObj'] = $tableObj;
        $response->getBody()->write($this->twig->render('displayItemsPage.twig',$this->renderArr));
        return $response;
    }

    public function post(Request $request,Response $response){
        
        return $response;
    }

    public function getCarbContent(Item $item):string{
        $carb = $item->getCarb();
        $sugar = $carb->getSugar();
        $fiber = $carb->getFiber();
        $other = $carb->getOther();
        $content = "Sugar: $sugar" . " g, Fiber: $fiber" . " g, Other: $other" . " g";
        return $content;
    }

    public function getFatContent(Item $item):string{
        $fat = $item->getFat();
        $saturated = $fat->getSaturated();
        $unsaturated = $fat->getUnsaturated();
        $content = "Saturated: $saturated" . " g, Unsaturated: $unsaturated" . " g";
        return $content;
    }

    public function getNameContent(Item $item):string{
        return $item->getName();
    }

    public function getCaloriesContent(Item $item):string{
        return strval($item->getCalories());
    }

    public function getProteinContent(Item $item):string{
        return strval($item->getProtein()) . " g";
    }

    public function getFoodUnitContent(Item $item):string{
        return $item->getFoodUnit();
    }

    public function getServingQtyContent(Item $item):string{
        return strval($item->getServingQty());
    }

}