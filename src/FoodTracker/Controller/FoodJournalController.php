<?php

namespace FoodTracker\Controller;

use Twig_Environment;
use FoodTracker\Persistence\Repo\iFoodJournalsRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class FoodJournalController
{

    /**
     * @var iFoodJournalsRepository
     */
    private iFoodJournalsRepository $foodJournalRepo;

    /**
     * @var array $renderArr
     */
    private $renderArr;

    /**
     * @var Twig_Environment
     */
    private $twig;

    public function __construct(Twig_Environment $twig,iFoodJournalsRepository $foodJournalRepo)
    {
        $this->twig = $twig;
        $this->foodJournalRepo = $foodJournalRepo;
    }

    public function get(Request $request,Response $response){
        $params = $request->getQueryParams();
        if(isset($params) and isset($params['date'])){
            $date = $params['date'];

            $date = str_replace('-','');

            $this->renderArr['dateChosen'] = true;
            $entries = $foodJournalRepo->getFoodJournalEntriesForDate($_SESSION['username'],);
            //Pass foodjournalentries to view
            $response->getBody()->write($this->twig->render('foodJournals.twig',$this->renderArr));
        }
        else{
            //View has a calendar to chose dates
            $this->renderArr['dateChosen'] = false;
            $response->getBody()->write($this->twig->render('foodJournals.twig',$this->renderArr));
        }

        return $response;
    }

}
