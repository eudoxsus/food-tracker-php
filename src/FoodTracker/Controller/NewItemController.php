<?php

namespace FoodTracker\Controller;

use FoodTracker\Persistence\Repo\iItemRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use FoodTracker\Model\Item;
use FoodTracker\Model\ItemBuilder;
use FoodTracker\Model\Fat;
use FoodTracker\Model\FatBuilder;
use FoodTracker\Model\Carb;
use FoodTracker\Model\CarbBuilder;
use Twig_Environment;

/**
 * ItemController
 * Controller for Items
 */
class NewItemController{

    private iItemRepository $itemRepo;

    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var array $renderArr
     */
    private $renderArr = [];

    public function __construct(Twig_Environment $twig,iItemRepository $itemRepo){
        $this->itemRepo = $itemRepo;
        $this->twig = $twig;
    }

    public function get(Request $request,Response $response){
        //$this->itemRepo->getItemsQuery('');
        $response->getBody()->write($this->twig->render('newItem.twig',$this->renderArr));
        return $response;
    }

    public function post(Request $request,Response $response){
        $formBody = $request->getParsedBody();
    
        $userName = $_SESSION['username'];
        $name = $formBody['name'];
        $calories = $formBody['calories'];
        $protein = $formBody['protein'];
        $servingQty = $formBody['servingQty'];
        $foodUnit = $formBody['foodUnit'];
        $newCarb = $this->buildNewCarb($formBody);
        $newFat = $this->buildNewFat($formBody);
        
        $itemBuilder = new ItemBuilder($newCarb,$newFat);
        $itemBuilder->setName($name)->setCalories($calories)->setProtein($protein)->setFoodUnit($foodUnit)->setServingQty($servingQty);
        $pendingItem = new Item($itemBuilder);

        $resArr = $this->itemRepo->insertItem($pendingItem);
        if($resArr['isSuccessful']){
            $this->renderArr['showToast'] = true;
            $this->renderArr['toastHeaderMsg'] = 'Success!';
            $this->renderArr['toastBodyMsg'] = "Item named '$name' was created successfully";
        }
        else{
            
        }

        $response->getBody()->write($this->twig->render('newItem.twig',$this->renderArr));
        return $response;
    }

    private function buildNewCarb(array $formBody){

        $fiber = $formBody['fiber'];
        $sugar = $formBody['sugar'];
        $other = $formBody['other'];

        $carbBuilder = new CarbBuilder();
        $carbBuilder->setFiber($fiber)->setSugar($sugar)->setOther($other);
        return new Carb($carbBuilder);
    }

    private function buildNewFat(array $formBody){

        $saturated = $formBody['saturated'];
        $unsaturated = $formBody['unsaturated'];

        $fatBuilder = new FatBuilder();
        $fatBuilder->setSaturated($saturated)->setUnsaturated($unsaturated);
        return new Fat($fatBuilder);
    }


}