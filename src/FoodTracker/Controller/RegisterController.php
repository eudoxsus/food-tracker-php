<?php

namespace FoodTracker\Controller;

use FoodTracker\Persistence\Repo\iUserRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Routing\RouteContext;
use FoodTracker\Routing\RoutingConstants;
use Twig_Environment;

class RegisterController{

    /**
     * @var iUserRepository User Repository object
     */
    private iUserRepository $userRepo;

    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var array $renderArr
     */
    private $renderArr;

    private function initRenderArr(Request $request){
        $routeParser = RouteContext::fromRequest($request)->getRouteParser();
        $this->renderArr = [ 'loginURL' => $routeParser->urlFor(RoutingConstants::LOGIN_GET_NAME) ];
    }

    public function __construct(Twig_Environment $twig,iUserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
        $this->twig = $twig;
    }
    
    public function get(Request $request,Response $response){
        $this->initRenderArr($request);
        $response->getBody()->write($this->twig->render('register.twig',$this->renderArr));
        return $response;
    }

    public function post(Request $request,Response $response){
        $this->initRenderArr($request);
        $formBody = $request->getParsedBody();

        $userName = $formBody['username'];
        $email = $formBody['email'];
        $reqPassword = $formBody['password'];

        if(!isset($userName,$email,$reqPassword)){
            $this->renderArr['userInvalid'] = true;
            $this->renderArr['userInvalidMsg'] = 'Please enter a username';
            $this->renderArr['passInvalid'] = true;
            $this->renderArr['passInvalidMsg'] = 'Please enter a password';
            $this->renderArr['emailInvalid'] = true;
            $this->renderArr['emailInvalidMsg'] = 'Please enter an email address';
            $response->getBody()->write($this->twig->render('register.twig',$this->renderArr));
            return $response;
        }

        $user = $this->userRepo->getUserByName($userName);
        if (!isset($user)){
            $hashedPass = password_hash($reqPassword,PASSWORD_DEFAULT);
            $resArr = $this->userRepo->insertUser($userName,$hashedPass,$email);
            if($resArr['isSuccessful']){
                $_SESSION['loggedin'] = TRUE;
                $_SESSION['username'] = $userName;

                $routeParser = RouteContext::fromRequest($request)->getRouteParser();
                $url = $routeParser->urlFor(RoutingConstants::HOME_GET_NAME);

                $response = $response->withHeader('Location',$url)->withStatus(302);
            }
            else{
                $errCode = $resArr['errorCode'];
                $this->renderArr['dbError'] = true;
                $this->renderArr['dbErrorMsg'] = "Database error occurred with the server, SQLi error: $errCode";
                $response->getBody()->write($this->twig->render('register.twig',$this->renderArr));
            }
        }
        else {
            $this->renderArr['userInvalid'] = true;
            $this->renderArr['userInvalidMsg'] = 'This username already exists, please choose another';
            $response->getBody()->write($this->twig->render('register.twig',$this->renderArr));
        }
        
        return $response;
    }

}