<?php

namespace FoodTracker\Controller;

use FoodTracker\Persistence\Repo\iUserRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Routing\RouteContext;
use Twig_Environment;
use FoodTracker\Controller\BaseTwigController;
use FoodTracker\Routing\RoutingConstants;

class LoginController{

    /**
     * @var iUserRepository
     */
    private iUserRepository $userRepo;

    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var array $renderArr
     */
    private $renderArr = [];

    public function __construct(Twig_Environment $twig,iUserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
        $this->twig = $twig;
    }

    private function initRenderArr(Request $request){
        $routeParser = RouteContext::fromRequest($request)->getRouteParser();
        if($request->getAttribute('accessDenied') !== null){
            $this->renderArr['accessDenied'] = $request->getAttribute('accessDenied');
            $this->renderArr['accessDeniedMsg'] = "Fuck you, you're not logged in";
        }
        $this->renderArr['registerURL'] = $routeParser->urlFor(RoutingConstants::REGISTER_GET_NAME);
    }
    
    public function get(Request $request,Response $response){
        $this->initRenderArr($request);
        if(isset($_SESSION['username'])){
            $this->renderArr['username'] = $_SESSION['username'];
        }

        $response->getBody()->write($this->twig->render('login.twig',$this->renderArr));
        return $response;
    }

    public function post(Request $request,Response $response){
        $this->initRenderArr($request);

        $formBody = $request->getParsedBody();

        $userName = $formBody['username'];
        $reqPassword = $formBody['password'];

        if(!isset($userName,$reqPassword)){
            $this->renderArr['userInvalid'] = true;
            $this->renderArr['userInvalidMsg'] = 'Please enter a username';
            $this->renderArr['passInvalid'] = true;
            $this->renderArr['passInvalidMsg'] = 'Please enter a password';
            $response->getBody()->write($this->twig->render('login.twig',$this->renderArr));
            return $response;
        }

        $_SESSION['username'] = $userName;
        $this->renderArr['username'] = $userName;


        $user = $this->userRepo->getUserByName($userName);
        if (isset($user)){

            if(password_verify($reqPassword,$user->getHashedPassword())){
                $_SESSION['loggedin'] = TRUE;

                $routeParser = RouteContext::fromRequest($request)->getRouteParser();
                $url = $routeParser->urlFor(RoutingConstants::HOME_GET_NAME);

                $response = $response->withHeader('Location',$url)->withStatus(302);
            } 
            else {
                $this->renderArr['passInvalid'] = true;
                $this->renderArr['passInvalidMsg'] = 'Incorrect password';
                $response->getBody()->write($this->twig->render('login.twig',$this->renderArr));
            }
        }
        else {
            $this->renderArr['userInvalid'] = true;
            $this->renderArr['userInvalidMsg'] = 'User with this name was not found';
            $response->getBody()->write($this->twig->render('login.twig',$this->renderArr));
        }
        
        return $response;
    }

}