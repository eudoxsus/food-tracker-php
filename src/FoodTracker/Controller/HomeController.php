<?php

namespace FoodTracker\Controller;

use Twig_Environment;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use FoodTracker\Routing\RoutingConstants;

class HomeController
{

    /**
     * @var array $renderArr
     */
    private $renderArr = [];

    /**
     * @var Twig_Environment
     */
    private $twig;

    public function __construct(Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    public function get(Request $request,Response $response){

        $this->renderArr['newItemsURL'] = RoutingConstants::ALL_ROUTES[RoutingConstants::NEW_ITEM_GET_NAME];
        $this->renderArr['displayItemsURL'] = RoutingConstants::ALL_ROUTES[RoutingConstants::DISPLAY_ITEM_GET_NAME];
        $this->renderArr['foodJournalsURL'] = RoutingConstants::ALL_ROUTES[RoutingConstants::FOODJOURNAL_GET_NAME];

        $response->getBody()->write($this->twig->render('home.twig',$this->renderArr));
        return $response;
    }

}
