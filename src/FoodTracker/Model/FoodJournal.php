<?php

namespace FoodTracker\Model;

/**
 * 
 */
class FoodJournal{

    private int $id;

    private int $userId;

    private int $date;

    public function __construct(int $id,int $userId,int $date){
        $this->$id = $id;
        $this->$userId = $userId;
        $this->$date = $date;
    }

    public function getId(){
        return $this->id;
    }

    public function getUserId(){
        return $this->userId;
    }

    public function getDate(){
        return $this->date;
    }

}

class FoodJournalEntry{

    private int $id;

    private FoodJournal $foodJournal;

    private int $entityId;

    private string $type;

    public function __construct(FoodJournalEntryBuilder $builder){
        $this->id = $builder->getId();
        $this->foodJournal = $builder->getFoodJournal();
        $this->entityId = $builder->getEntityId();
        $this->type = $builder->getType();
    }

    public function getId(){
        return $this->id;
    }

    public function getEntityId(){
        return $this->entityId;
    }

    public function getFoodJournal(){
        return $this->foodJournal;
    }

    public function getType(){
        return $this->type;
    }
}

class FoodJournalEntryBuilder{

    private int $id;

    private FoodJournal $foodJournal;

    private int $entityId;

    private string $type;

    public function __construct($foodJournal){
        $this->foodJournal = $foodJournal;
    }

    public function getId(){
        return $this->id;
    }

    public function getEntityId(){
        return $this->entityId;
    }

    public function getFoodJournal(){
        return $this->foodJournal;
    }

    public function getType(){
        return $this->type;
    }

    public function setId(int $id){
        $this->id = $id;
        return $this;
    }

    public function setEntityId(int $entityId){
        $this->entityId = $entityId;
        return $this;
    }

    public function setType(string $type){
        $this->type = $type;
        return $this;
    }

}