<?php

namespace FoodTracker\Model;

/**
 * Represents a User
 */
class User{

    private int $id;

    private string $username;

    private string $email;
    
    public function __construct($id,$username,$email,$hashedPassword){
        $this->id = $id;
        $this->username = $username;
        $this->email = $email;
        $this->hashedPassword = $hashedPassword;
    }

    public function getId(){
        return $this->id;
    }

    public function getUsername(){
        return $this->username;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getHashedPassword(){
        return $this->hashedPassword;
    }

}