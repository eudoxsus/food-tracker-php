<?php

namespace FoodTracker\Model;

use FoodTracker\Model\Carb;
use FoodTracker\Model\Fat;
use FoodTracker\GeneralHelpers;

/**
 * Represents a single unit of food
 */
class Item implements \ArrayAccess{

    private int $id;
    private Carb $carb;
    private Fat $fat;
    private string $name;
    private string $source;
    private float $calories;
    private float $protein;
    private string $foodUnit;
    private float $servingQty;

    const GET_METHOD_REFS = [
        'carbId' => 'getCarbId',
        'fatId' => 'getFatId',
        'name' => 'getName',
        'calories' => 'getCalories',
        'protein' => 'getProtein',
        'foodUnit' => 'getFoodUnit',
        'servingQty' => 'getServingQty'
    ];

    public function __construct(ItemBuilder $builder){
        $this->id = $builder->getId();
        $this->carb = $builder->getCarb();
        $this->fat = $builder->getFat();
        $this->name = $builder->getName();
        $this->calories = $builder->getCalories();
        $this->protein = $builder->getProtein();
        $this->foodUnit = $builder->getFoodUnit();
        $this->servingQty = $builder->getServingQty();

    }

    public function getId():int{
        return $this->id;
    }

    public function getCarb():Carb{
        return $this->carb;
    }

    public function getCarbId():int{
        return $this->carb->getId();
    }

    public function getFat():Fat{
        return $this->fat;
    }

    public function getFatId():int{
        return $this->fat->getId();
    }

    public function getName():string{
        return $this->name;
    }

    public function getSource(){
        return $this->source;
    }

    public function getCalories(){
        return $this->calories;
    }

    public function getProtein(){
        return $this->protein;
    }

    public function getFoodUnit(){
        return $this->foodUnit;
    }

    public function getServingQty(){
        return $this->servingQty;
    }

    public function offsetSet($offset, $value) {
        /*
        if (is_null($offset)) {
            $this->methodRefs[] = $value;
        } else {
            $this->methodRefs[$offset] = $value;
        }*/
    }

    public function offsetExists($offset) {
        return isset(Item::GET_METHOD_REFS[$offset]);
    }

    public function offsetUnset($offset) {
        //unset($this->methodRefs[$offset]);
    }

    public function offsetGet($offset) {
        if(isset($this->methodRefs[$offset])){
            return $this->{Item::GET_METHOD_REFS[$offset]}();
            //return call_user_func(array($this, Item::GET_METHOD_REFS[$offset]));
        }
        return null;
    }

}

