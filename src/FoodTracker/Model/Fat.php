<?php

namespace FoodTracker\Model;

use FoodTracker\Model\Item;

/**
 * Holds the Fat information of a food Item
 */
class Fat{
    private int $id;
    private int $foodId;
    private float $saturated;
    private float $unsaturated;

    public function __construct(FatBuilder $fatBuilder){
        $this->id = $fatBuilder->getId();
        $this->foodId = $fatBuilder->getFoodId();
        $this->saturated = $fatBuilder->getSaturated();
        $this->unsaturated = $fatBuilder->getUnsaturated();
    }

    public function getId(){
        return $this->id;
    }

    public function getFoodId(){
        return $this->foodId;
    }

    public function getSaturated(){
        return $this->saturated;
    }

    public function getUnsaturated(){
        return $this->unsaturated;
    }

}