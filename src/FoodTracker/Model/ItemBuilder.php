<?php

namespace FoodTracker\Model;

use FoodTracker\Model\Carb;
use FoodTracker\Model\Fat;

class ItemBuilder implements \ArrayAccess{

    private int $id = -1;
    private ?Carb $carb;
    private ?Fat $fat;
    private string $name;
    private string $source;
    private float $calories;
    private float $protein;
    private float $servingQty;
    private string $foodUnit;

    public function __construct($carb,$fat){
        $this->carb = $carb;
        $this->fat = $fat;
    }

    public function offsetSet($offset, $value) {
        if (!is_null($offset)) {
            //call_user_func(array($this, ItemBuilder::SET_METHOD_REFS[$offset],$value));
            $this->{ItemBuilder::SET_METHOD_REFS[$offset]}($value);
        }
    }

    public function offsetExists($offset) {
        return isset(ItemBuilder::SET_METHOD_REFS[$offset]);
    }

    const SET_METHOD_REFS = [
        'id' => 'setId',
        'name' => 'setName',
        'calories' => 'setCalories',
        'protein' => 'setProtein',
        'foodUnit' => 'setFoodUnit',
        'servingQty' => 'setServingQty'
    ];

    public function getId(){
        return $this->id;
    }

    public function getCarb(){
        return $this->carb;
    }

    public function getFat(){
        return $this->fat;
    }

    public function getName(){
        return $this->name;
    }

    public function getSource(){
        return $this->source;
    }

    public function getCalories(){
        return $this->calories;
    }

    public function getProtein(){
        return $this->protein;
    }

    public function getFoodUnit(){
        return $this->foodUnit;
    }

    public function getServingQty(){
        return $this->servingQty;
    }

    public function setId(int $id):ItemBuilder{
        $this->id = $id;
        return $this;
    }

    public function setCarb(Carb $carb):ItemBuilder{
        $this->carb = $carb;
        return $this;
    }

    public function setFat(Fat $fat):ItemBuilder{
        $this->fat = $fat;
        return $this;
    }

    public function setName(string $name):ItemBuilder{
        $this->name = $name;
        return $this;
    }

    public function setSource(string $source):ItemBuilder{
        $this->source = $source;
        return $this;
    }

    public function setCalories(float $calories):ItemBuilder{
        $this->calories = $calories;
        return $this;
    }

    public function setProtein(float $protein):ItemBuilder{
        $this->protein = $protein;
        return $this;
    }

    public function setFoodUnit(string $foodUnit):ItemBuilder{
        $this->foodUnit = $foodUnit;
        return $this;
    }

    public function setServingQty(float $servingQty):ItemBuilder{
        $this->servingQty = $servingQty;
        return $this;
    }

    public function copyItem(Item $item):ItemBuilder{
        $this->id = $item->getId();
        $this->name = $item->getName();
        $this->calories = $item->getCalories();
        $this->protein = $item->getProtein();
        $this->servingQty = $item->getServingQty();
        $this->foodUnit = $item->getFoodUnit();
        return $this;
    }

    public function clear(){
        $this->carb = null;
        $this->fat = null;
        $this->id = -1;
        $this->name = '';
        $this->calories = 0;
        $this->protein = 0;
        $this->servingQty = 0;
        $this->foodUnit = '';
    }



    public function offsetUnset($offset) {
        //unset($this->methodRefs[$offset]);
    }

    public function offsetGet($offset) {
        /*
        if(isset($this->methodRefs[$offset])){
            return call_user_func(array($this, Item::METHOD_REFS[$offset],));
        }
        return null;*/
    }

}