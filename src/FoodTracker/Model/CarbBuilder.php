<?php

namespace FoodTracker\Model;

class CarbBuilder{

private int $id = -1;
private int $foodId = -1;
private float $fiber;
private float $sugar;
private float $other;

public function __construct(){

}

public function getId(){
    return $this->id;
}

public function getFoodId(){
    return $this->foodId;
}

public function getFiber(){
    return $this->fiber;
}

public function getSugar(){
    return $this->sugar;
}

public function getOther(){
    return $this->other;
}

public function setId(int $id){
    $this->id = $id;
    return $this;
}

public function setFoodId(int $foodId){
    $this->foodId = $foodId;
    return $this;
}

public function setFiber(float $fiber){
    $this->fiber = $fiber;
    return $this;
}

public function setSugar(float $sugar){
    $this->sugar = $sugar;
    return $this;
}

public function setOther(float $other){
    $this->other = $other;
    return $this;
}

public function copyCarb(Carb $carb):CarbBuilder{
    $this->id = $carb->getId();
    $this->foodId = $carb->getFoodId();
    $this->fiber = $carb->getFiber();
    $this->sugar = $carb->getSugar();
    $this->other = $carb->getOther();
    return $this;
}

}