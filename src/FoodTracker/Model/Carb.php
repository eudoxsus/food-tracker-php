<?php

namespace FoodTracker\Model;

/**
 * Holds the Carbohydrate information of a food Item
 */
class Carb{
    private int $id;
    private int $foodId;
    private float $fiber;
    private float $sugar;
    private float $other;

    public function __construct(CarbBuilder $builder){
        $this->id = $builder->getId();
        $this->foodId = $builder->getFoodId();
        $this->fiber = $builder->getFiber();
        $this->sugar = $builder->getSugar();
        $this->other = $builder->getOther();
    }

    public function getId(){
        return $this->id;
    }

    public function getFoodId(){
        return $this->foodId;
    }

    public function getFiber(){
        return $this->fiber;
    }

    public function getSugar(){
        return $this->sugar;
    }

    public function getOther(){
        return $this->other;
    }

}

