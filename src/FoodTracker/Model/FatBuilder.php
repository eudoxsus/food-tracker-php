<?php

namespace FoodTracker\Model;

class FatBuilder{

private int $id = -1;
private int $foodId = -1;
private float $saturated;
private float $unsaturated;

public function __construct(){

}

public function getId(){
    return $this->id;
}

public function getFoodId(){
    return $this->foodId;
}

public function getSaturated(){
    return $this->saturated;
}

public function getUnsaturated(){
    return $this->unsaturated;
}

public function setId(int $id){
    $this->id = $id;
    return $this;
}

public function setFoodId(int $foodId){
    $this->foodId = $foodId;
    return $this;
}

public function setSaturated(float $saturated){
    $this->saturated = $saturated;
    return $this;
}

public function setUnsaturated(float $unsaturated){
    $this->unsaturated = $unsaturated;
    return $this;
}

public function copyFat(Fat $fat){
    $this->id = $fat->getId();
    $this->foodId = $fat->getFoodId();
    $this->unsaturated = $fat->getUnsaturated();
    $this->saturated = $fat->getSaturated();
    return $this;
}


}